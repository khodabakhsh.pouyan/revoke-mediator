package ir.smartpath.ac.mediators.revoke;

import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import ir.smartpath.ac.mediators.revoke.client.LettuceInitializer;
import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;

public class RevokeMediator extends AbstractMediator {
    private String key;


    @Override
    public boolean mediate(MessageContext synCtx) {
        LettuceInitializer lettuceInitializer = LettuceInitializer.getInstance();
        try (StatefulRedisConnection<String, String> connection = lettuceInitializer.getPool().borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            boolean keyExists = false;
            if (commands.exists(getKey()) > 0) {
                keyExists = true;
            }
            synCtx.setProperty("exist", keyExists);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return true;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
