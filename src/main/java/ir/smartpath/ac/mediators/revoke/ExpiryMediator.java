package ir.smartpath.ac.mediators.revoke;

import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import ir.smartpath.ac.mediators.revoke.client.LettuceInitializer;
import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ExpiryMediator extends AbstractMediator {

    private String key;
    private String value;

    @Override
    public boolean mediate(MessageContext synCtx) {

        LettuceInitializer lettuceInitializer = LettuceInitializer.getInstance();
        try (StatefulRedisConnection<String, String> connection = lettuceInitializer.getPool().borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();

            ZonedDateTime timeNow = ZonedDateTime.now(ZoneId.of("Asia/Tehran"));
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS Z");
            ZonedDateTime expTime = ZonedDateTime.parse(getValue(), dateTimeFormatter);
            commands.setex("key", Duration.between(timeNow, expTime).getSeconds(), getKey());
            if (log.isDebugEnabled()){
                log.debug("property has been successfully set into redis");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }

        return true;
    }

//    public static void main(String[] args) {
//        LettuceInitializer lettuceInitializer = LettuceInitializer.getInstance();
//        try (StatefulRedisConnection<String, String> connection = lettuceInitializer.getPool().borrowObject()) {
//            RedisCommands<String, String> commands = connection.sync();
//
//            ZonedDateTime timeNow = ZonedDateTime.now(ZoneId.of("Asia/Tehran"));
//            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS Z");
//            ZonedDateTime expTime = ZonedDateTime.parse("getValue()", dateTimeFormatter);
//            commands.setex("key", Duration.between(timeNow, expTime).getSeconds(), "getKey()");
//
//        } catch (Exception e) {
////            log.error(e.getMessage());
////            return false;
//        }
//    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
