package ir.smartpath.ac.mediators.revoke.client;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.support.ConnectionPoolSupport;
import ir.smartpath.ac.mediators.revoke.conf.ConfigReader;
import ir.smartpath.ac.mediators.revoke.conf.UrlDTO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import java.io.FileNotFoundException;

public class LettuceInitializer {

    private static final Log log = LogFactory.getLog(LettuceInitializer.class);
    private static GenericObjectPool<StatefulRedisConnection<String, String>> pool;
    private static final RedisClient redisClient;
    private static final LettuceInitializer instance = new LettuceInitializer();


    private LettuceInitializer() {
    }

    static {
        UrlDTO urlProps = null;
        String path =  "/revoke-mediator-config.json";
        try {
            urlProps = ConfigReader.readConfiguration(path);
        } catch (FileNotFoundException e) {
            log.error("there was a problem reading configs from file");
        }
        assert urlProps != null;
        redisClient = RedisClient.create(RedisURI.create(urlProps.getHost(), urlProps.getPort()));
        GenericObjectPoolConfig<StatefulRedisConnection<String, String>> poolConfig = new GenericObjectPoolConfig<>();
        poolConfig.setMaxTotal(100);
        poolConfig.setMaxIdle(10);
        poolConfig.setMinIdle(10);
        pool = ConnectionPoolSupport
                .createGenericObjectPool(redisClient::connect, poolConfig);
    }

    public GenericObjectPool<StatefulRedisConnection<String, String>> getPool() {
        return pool;
    }

    public void setPool(GenericObjectPool<StatefulRedisConnection<String, String>> pool) {
        this.pool = pool;
    }

    public static LettuceInitializer getInstance(){
        return instance;
    }
}
