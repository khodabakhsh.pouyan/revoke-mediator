

  ```xml
 <sequence xmlns="http://ws.apache.org/ns/synapse" name="oauth-sequence">
     <log level="custom">
         <property name="TRACE" value="API Mediation Policy"/>
     </log>
     <class name = "org.wso2.apim.mediators.oauth.OAuthMediator">
         <property name="firstEndpointId" value="EP1"></property>
         <property name="SecondEndpointId" value="EP2"></property>
     </class>
 </sequence>